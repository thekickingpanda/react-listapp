import React, { Component } from 'react'
import FaEdit from 'react-icons/lib/fa/edit'
import FaBomb from 'react-icons/lib/fa/bomb'
import FaCheck from 'react-icons/lib/fa/check'
import FaAmbulance from 'react-icons/lib/fa/ambulance'
import FaSnapchatGhost from 'react-icons/lib/fa/snapchat-ghost'

class Card extends Component {
  constructor() {
    super();
    this.state = {
      currentEdit: ""
    };
    this.handleKeyPress = this.handleKeyPress.bind(this)
  }

  handleKeyPress(e) {
    if (e.key === 'Enter') {
      this.props.todo.status = "todo"
      this.props.todo.desc = this.state.currentEdit
      this.setState({ todos: this.props.todo, currentEdit: "" })
    }
  }

  onInputChange = e => {
    this.setState({ currentEdit: e.target.value })
  }

  editTodo = () => {
    this.props.todo.status = "edit"
    this.setState({ todos: this.props.todo })
  }

  editIsDone = () => {
    this.props.todo.status = "todo"
    this.props.todo.desc = this.state.currentEdit
    this.setState({ todos: this.props.todo, currentEdit: "" })
  }

  renderDone() {
    return (
      <div className="card text-white bg-success">
        <p className="card-title text-center">{this.props.todo.desc}</p>
        <span className="hidden-note">
            <button id="remove" onClick={this.props.delete}><FaBomb /></button>
          </span>
      </div>
    );
  }
  renderEdit() {
    return (
      <div className="card text-white bg-secondary">
        <input type="text" className="form-control bg-warning" placeholder={this.props.todo.desc} onChange={this.onInputChange} onKeyPress={this.handleKeyPress}/>
        <button id="done" onClick={this.editIsDone} >OK</button>
      </div>
    );
  }
  renderOmfg() {
    return (
      <div className="card text-white bg-danger">
        <p className="card-title text-center">{this.props.todo.desc}</p>
        <span className="hidden-note">
            <button id="done" onClick={this.props.done}><FaCheck /></button>
            <button id="edit" onClick={this.editTodo}><FaEdit /></button>
            <button id="remove" onClick={this.props.delete}><FaBomb /></button>
          </span>
      </div>
    );
  }
  renderGhost() {
    return (
      <div className="card text-muted bg-light">
        <p className="card-title text-center">{this.props.todo.desc}</p>
        <span className="hidden-note">
          <button id="remove" onClick={this.props.delete}><FaBomb /></button>
        </span>
      </div>
    );
  }
  renderTodo() {
    return (
      <div className="card highlighted">
        <p className="card-title text-center">{this.props.todo.desc}</p>
        <span className="hidden-note">
            <button id="done" onClick={this.props.done}><FaCheck /></button>
            <button id="edit" onClick={this.editTodo}><FaEdit /></button>
            <button id="remove" onClick={this.props.delete}><FaBomb /></button>
            <button id="omfg" onClick={this.props.omfg}><FaAmbulance /></button>
            <button id="ghost" onClick={this.props.ghost}><FaSnapchatGhost /></button>
          </span>
      </div>
    );
  }

  render() {
    if (this.props.todo.status === "done") {
      return this.renderDone()
    } else if (this.props.todo.status === "edit") {
      return this.renderEdit()
    } else if (this.props.todo.status === "omfg") {
      return this.renderOmfg()
    } else if (this.props.todo.status === "ghost") {
      return this.renderGhost()
    } else {
      return this.renderTodo()
    }
  }
}

export default Card;

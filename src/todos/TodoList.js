import React, { Component } from 'react';
import Card from "./Card"
import {TodoContext} from "./TodoContext"


class TodoList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <TodoContext.Consumer>
          {context => (
            <div className="card-columns">
              {context.todos.map((item, index) => {
                return (
                  <Card key={'todo' + index} todo={item}
                    delete={() => context.removeTodo(index)}
                    done={() => context.updateTodo(index, 'done')}
                    omfg={() => context.updateTodo(index, 'omfg')}
                    ghost={() => context.updateTodo(index, 'ghost')}
                    />
                )
              })}
            </div>
          )}
        </TodoContext.Consumer>
      </div>
    )
  }
}

export default TodoList;

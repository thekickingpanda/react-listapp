import React from 'react';

export const TodoContext = React.createContext({
  todos: [],
  loadTodos: () => {},
  addTodo: () => {},
  updateTodo: () => {},
  removeTodo: () => {},
})

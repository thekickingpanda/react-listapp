import React from 'react'


export const OwnerContext = React.createContext({
  baseurl: '',
  owner: "KWA",
  toggleOwner: () => {},
})

function OwnerTogglerButton() {
  return (
    <OwnerContext.Consumer>
      {({owner, toggleOwner}) => (
        <button onClick={toggleOwner}>
          {owner}
        </button>
      )}
    </OwnerContext.Consumer>
  )
}

export function OwnerSelector() {
  return (
    <div>
      <OwnerTogglerButton />
    </div>
  );
}

export function OwnerShow() {
  return (
    <OwnerContext.Consumer>
      {owner => (
        <h1>
          {owner.owner}'s To-Do List!
        </h1>
      )}
    </OwnerContext.Consumer>
  )
}

import React, { Component } from 'react';
import {OwnerShow, OwnerSelector} from './OwnerContext'


class OwnerHeader extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <OwnerShow />
        <OwnerSelector />
      </div>
    );
  }
}

export default OwnerHeader;

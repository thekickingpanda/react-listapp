import React, { Component } from 'react'
import {OwnerContext} from "../owner/OwnerContext"
import {TodoContext} from "../todos/TodoContext"


class NewCard extends Component {
  constructor(props) {
    super(props);

    this.addCardChange = e => {
      this.setState({ currentTodo: e.target.value })
    }

    this.keyAddCard = (e, baseurl, owner, addcard) => {
      if (e.key === 'Enter') {
        if (this.state.currentTodo.length > 0) {
          this.addCardAndReset(owner, baseurl, addcard)
        }
      }
    }

    this.clickAddCard = (owner, baseurl, addcard) => {
      if (this.state.currentTodo.length > 0)
        this.addCardAndReset(owner, baseurl, addcard)
    }

    this.addCardAndReset = (owner, baseurl, addcard) => {
      let newDesc = this.state.currentTodo
      addcard(newDesc)
      this.setState({ currentTodo: "" })
      var url = baseurl + '?a=' + owner + '&desc=' + newDesc
      fetch(url)
        .then((response) => {
            return response.text();
        })
        .then(resp => {
          if (resp !== 'OK') console.log('failed to add card')
        })
    }

    this.state = {
      currentTodo: "",
    }
  }

  render() {
    return (
      <div>
        <OwnerContext.Consumer>
          {owner => (
          <TodoContext.Consumer>
            {todo => (
              <div>
                <input className="form-control" placeholder="Enter todo"
                  value={this.state.currentTodo}
                  onChange={e => this.addCardChange(e)}
                  onKeyPress={e => this.keyAddCard(e, owner.baseurl, owner.owner, todo.addTodo)} />
                <div className="mt-2">
                  <span className="p-3">
                    <button className="btn btn-primary" onClick={e => this.clickAddCard(owner.baseurl, owner.owner, todo.addTodo)}>Add Todo</button>
                  </span>
                </div>
              </div>
            )}
          </TodoContext.Consumer>
          )}
        </OwnerContext.Consumer>
        {this.state.owner}
      </div>
    )
  }
}


export default NewCard

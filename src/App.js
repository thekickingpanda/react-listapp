import React, { Component } from 'react';
import './App.css';

import {OwnerContext} from "./owner/OwnerContext"
import {TodoContext} from "./todos/TodoContext"

import OwnerHeader from "./owner/OwnerHeader"
import NewCard from "./newcard/NewCard"
import TodoList from './todos/TodoList';


class App extends Component {
  constructor(props) {
    super(props);

    this.toggleOwner = () => {
      var newOwner = this.state.owner === 'KWA' ? 'vocab' : 'KWA'
      this.setState({ owner: newOwner })
      console.log('current owner is ' + newOwner)
      this.loadTodos(this.state.baseurl, newOwner)
    }

    this.loadTodos = (baseurl, owner) => {
      var url = baseurl + '?a=' +  owner
      fetch(url)
        .then(response => response.json())
        .then(json => {
          this.setState({ todos: json.todoList })
        })
    }

    this.addTodo = (newTodo) => {
      let todosCopy = this.state.todos.slice()
      todosCopy.push({status: "todo", desc: newTodo})
      this.setState({ todos: todosCopy })
    }

    this.removeTodo = i => {
      let todosCopy = this.state.todos.slice()
      todosCopy.splice(i, 1);
      this.setState({ todos: todosCopy })
      var url = this.state.baseurl + '?a=' + this.state.owner + '&ref=' + (i+1).toString() + '&comm=del'
      this.runAppScriptCommand(url)
    }

    this.updateTodo = (i, command) => {
      let todosCopy = this.state.todos.slice()
      todosCopy[i].status = command
      this.setState({ todos: todosCopy })
      var url = this.state.baseurl + '?a=' + this.state.owner + '&ref=' + (i+1).toString() + '&comm=' + command
      this.runAppScriptCommand(url)
    }

    this.runAppScriptCommand = (url) => {
      fetch(url)
        .then((response) => {
            return response.text();
        })
        .then(resp => {
          if (resp !== 'OK') console.log('failed to remove card')
        })
    }

    this.state = {
      baseurl: 'https://script.google.com/macros/s/AKfycbxdoQQVbAcWv9IElyh9krlUp5ldG00g1tURTPfxASSxvfdLPrg/exec',
      owner: "KWA",
      toggleOwner: this.toggleOwner,
      todos: [],
      loadTodos: this.loadTodos,
      addTodo: this.addTodo,
      updateTodo: this.updateTodo,
      removeTodo: this.removeTodo,
    }
  }

  componentDidMount() {
    this.loadTodos(this.state.baseurl, this.state.owner)
  }

  render() {
    return (
      <div>
        <OwnerContext.Provider value={this.state}>
          <OwnerHeader />

          <TodoContext.Provider value={this.state}>
            <NewCard />

            <TodoList />
          </TodoContext.Provider>        
        </OwnerContext.Provider>
      </div>
    )
  }
}


export default App;
